package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.Day;

import java.util.List;

public interface VisitCalendarFactory {

    Day createDay();

    List<Day> createDays(int x);

    List<Day> createRestOfCurrentMonthDays();

    List<Day> createDaysInChosenMonth(int month, int year);
}
