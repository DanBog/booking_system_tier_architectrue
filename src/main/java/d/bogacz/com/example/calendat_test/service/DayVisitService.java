package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.Day;
import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.repository.DayRepository;
import d.bogacz.com.example.calendat_test.repository.SpringJpaVisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class DayVisitService {

    @Autowired
    private DayService dayService;
    @Autowired
    private DayRepository dayRepository;
    @Autowired
    private SpringJpaVisitRepository springJpaVisitRepository;
    @Autowired
    private VisitService visitService;

    public void setVisitsToDays(List<Day> dayList) {

        for (Day day : dayList
        ) {
            dayService.setStartAndFinishVisitsTime(day);
            if (day.getStartVisitsTime() != null) {
                LocalDateTime start = day.getStartVisitsTime();
                LocalDateTime finish = day.getFinishVisitsTime();
                LocalDateTime finishTimeCurrentVisit = start;
                day.setVisitList(new ArrayList<>());
                do {
                    finishTimeCurrentVisit = finishTimeCurrentVisit.plusMinutes(30);
                    Visit visit = visitService.createVisit(start, finishTimeCurrentVisit);
                    day.getVisitList().add(visit);
                    springJpaVisitRepository.save(visit);
                    start = finishTimeCurrentVisit;

                } while (!finish.equals(finishTimeCurrentVisit));
                dayRepository.save(day);
            }
        }
    }
}

