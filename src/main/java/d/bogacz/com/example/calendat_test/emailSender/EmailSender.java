package d.bogacz.com.example.calendat_test.emailSender;

public interface EmailSender {
    void sendEmail(String to, String subject, String content);
}
