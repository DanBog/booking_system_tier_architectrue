package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.Role;
import d.bogacz.com.example.calendat_test.repository.RoleRepository;
import d.bogacz.com.example.calendat_test.service.exception.RoleNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RoleService {


    @Autowired
    private RoleRepository roleRepository;

    public Role createRole(String rola, int id) {
        Role role = new Role();
        role.setRole(rola);
        role.setRoleId(id);
        roleRepository.save(role);
        return role;
    }

    public Role findById(Integer id) throws RoleNotFoundException {
        return roleRepository.findById(id).orElseThrow(() -> new RoleNotFoundException(""));
    }
}


