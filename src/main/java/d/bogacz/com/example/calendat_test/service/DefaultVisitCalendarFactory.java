package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.Day;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Component
public class DefaultVisitCalendarFactory implements VisitCalendarFactory {

    @Autowired
    private final TimeProvider timeProvider;

    @Override
    public Day createDay() {
        Day day = new Day();
        day.setDate(timeProvider.now());
        return day;
    }

    @Override
    public List<Day> createDays(int x) {
        List<Day> dayList = new ArrayList<>();
        LocalDate localDate = LocalDate.now();
        for (int i = 0; i < x; i++) {
            localDate = localDate.plusDays(1);
            Day day = new Day();
            day.setDate(localDate);
            dayList.add(day);
        }
        return dayList;
    }

    @Override
    public List<Day> createRestOfCurrentMonthDays() {
        List<Day> daysInCurentMonthList = new ArrayList<>();
        LocalDate localDate = LocalDate.now();

        int curentMonth = localDate.getMonthValue();
        while (true) {
            localDate = localDate.plusDays(1);
            int monthFromData = localDate.getMonthValue();
            if (curentMonth == monthFromData) {
                Day day = new Day();
                day.setDate(localDate);
                daysInCurentMonthList.add(day);
            } else {
                break;
            }
        }
        return daysInCurentMonthList;
    }

    @Override
    public List<Day> createDaysInChosenMonth(int month, int year) {
        List<Day> daysInChosenMonth = new ArrayList<>();
        LocalDate localDate = LocalDate.now();
        localDate = localDate.of(year, month, 1);

        while (localDate.getMonthValue() == month) {
            Day day = new Day();
            day.setId(String.valueOf(UUID.randomUUID()));
            day.setDate(localDate);
            daysInChosenMonth.add(day);
            localDate = localDate.plusDays(1);
        }
        return daysInChosenMonth;
    }
}
