package d.bogacz.com.example.calendat_test.repository;

import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.model.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Component
public class SpringJpaBasedVisitRepository implements VisitRepository {

    @Autowired
    private SpringJpaVisitRepository springJpaVisitRepository;


    @Override
    public List<Visit> findVisitForSelectedDay(String dayId) {
        return springJpaVisitRepository.findVisitForSelectedDay(dayId);
    }

    @Override
    public List<Visit> findAllUnreservedVisit() {
        return springJpaVisitRepository.findAllUnreservedVisit();
    }

    @Override
    public void bookVisit(String userId, String visitId) {
        springJpaVisitRepository.bookVisit(userId, visitId);
    }

    @Override
    public void removeUserIdFromVisit(String userId) {
        springJpaVisitRepository.removeUserIdFromVisit(userId);
    }

    @Override
    public List<Visit> findVisitBookedByIndicatedUser(String userId) {
        return springJpaVisitRepository.findVisitBookedByIndicatedUser(userId);
    }

    @Override
    public Optional<Visit> findById(String visitId) {
        return springJpaVisitRepository.findById(visitId);
    }

    @Override
    public List<Visit> findAll() {
        return springJpaVisitRepository.findAll();
    }

    @Override
    public void delete(Visit visit) {
        springJpaVisitRepository.delete(visit);

    }

    @Override
    public Visit save(Visit visit) {
        return springJpaVisitRepository.save(visit);
    }

    @Override
    public List<Visit> findNextDayVisits() {
        LocalDateTime startTimeVisits = LocalDateTime.now().plusDays(1).withHour(0).withMinute(0).withSecond(0);
        LocalDateTime finishTimeVisits = LocalDateTime.now().plusDays(1).withHour(23).withMinute(0).withSecond(0);
        System.out.println(startTimeVisits.toString());
        System.out.println(finishTimeVisits.toString());
        return springJpaVisitRepository.findNextDayVisits(startTimeVisits, finishTimeVisits);
    }
}
