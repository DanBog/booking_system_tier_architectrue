package d.bogacz.com.example.calendat_test.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateVisitDTO {

    private LocalDateTime startTime;
    private LocalDateTime finishTime;
    private String userId;


}
