package d.bogacz.com.example.calendat_test.service;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class SystemTimeProvider implements TimeProvider {

    @Override
    public LocalDate now() {
        return LocalDate.now();
    }
}
