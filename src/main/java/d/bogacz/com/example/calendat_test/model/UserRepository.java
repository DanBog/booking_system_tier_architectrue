package d.bogacz.com.example.calendat_test.model;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    User save(User user);

    List<User> findAll();

    Optional<User>findById(String id);

    void delete(User user);
}
