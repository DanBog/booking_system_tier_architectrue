package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.Day;
import d.bogacz.com.example.calendat_test.repository.DayRepository;
import d.bogacz.com.example.calendat_test.service.exception.DayNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class DayService {


    @Autowired
    private DayRepository dayRepository;

    public List<Day> getAllDays() {
        return dayRepository.findAll();
    }

    public Day findDayById(String id) throws DayNotFoundException {
        Optional<Day> d = dayRepository.findById(id);
        return d.orElseThrow(DayNotFoundException::new);
    }

    public void setStartAndFinishVisitsTime(Day day) {
        LocalDate localDate = day.getDate();
        String dayOfWeek = String.valueOf(localDate.getDayOfWeek());
        if (!dayOfWeek.equals("SATURDAY") && !dayOfWeek.equals("SUNDAY")) {
            day.setStartVisitsTime(LocalDateTime.of(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth(), 9, 0));
            day.setFinishVisitsTime(LocalDateTime.of(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth(), 11, 0));
        }
    }
}