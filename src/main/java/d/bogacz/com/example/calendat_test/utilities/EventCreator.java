package d.bogacz.com.example.calendat_test.utilities;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class EventCreator {

    @Scheduled(fixedRate = 10000)
    public void create() {
        final LocalDateTime start = LocalDateTime.now();
        System.out.println("Event creator at time: " + start);
    }
}
