package d.bogacz.com.example.calendat_test.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Builder
public class Visit {

    @Id
    private String visitId = String.valueOf(UUID.randomUUID());
    private LocalDateTime startTime;
    private LocalDateTime finishTime;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Visit(LocalDateTime statTime, LocalDateTime finishTime) {
        this.startTime = statTime;
        this.finishTime = finishTime;
    }


    public Visit(LocalDateTime statTime, LocalDateTime finishTime, Visit byId) {
        this.startTime = statTime;
        this.startTime = finishTime;

    }
}

