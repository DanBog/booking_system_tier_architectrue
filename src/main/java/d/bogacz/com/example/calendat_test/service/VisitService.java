package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.model.VisitRepository;
import d.bogacz.com.example.calendat_test.service.dto.CreateVisitDTO;
import d.bogacz.com.example.calendat_test.service.dto.VisitDTO;
import d.bogacz.com.example.calendat_test.service.exception.VisitNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class VisitService {

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private VisitDTOMaper mapper;

    public List<Visit> getVisitsFromSelectedDay(String dayId) {
        return visitRepository.findVisitForSelectedDay(dayId);

    }

    public Visit createVisit(LocalDateTime startTime, LocalDateTime finishTime) {
        Visit visit = new Visit(startTime, finishTime);
        visit.setVisitId(String.valueOf(UUID.randomUUID()));
        return visit;
    }


    public List<Visit> getAllUnreservedVisits() {
        return visitRepository.findAllUnreservedVisit();
    }

    @Transactional
    public VisitDTO bookVisit(String userId, String visitId) {
        visitRepository.bookVisit(userId, visitId);
        Visit visit = visitRepository.findById(visitId).orElseThrow(() -> new VisitNotFoundException());
        return mapper.visitTOVisitDTO(visit);
    }

    public List<VisitDTO> getAllVisits() {
        List<Visit> visitList = visitRepository.findAll();
        return visitList.stream().map(v -> mapper.visitTOVisitDTO(v)).collect(Collectors.toList());

    }

    public VisitDTO getVisitByLogin(String visitId) {
        return visitRepository.findById(visitId).map(v -> mapper.visitTOVisitDTO(v)).orElseThrow(() -> new VisitNotFoundException());
    }


    public VisitDTO deleteVisitByLogin(String visitId) {
        Visit visit = visitRepository.findById(visitId).orElseThrow(() -> new VisitNotFoundException());
        visitRepository.delete(visit);
        return mapper.visitTOVisitDTO(visit);
    }


    public void removeUserFromVisit(String userId) {
        visitRepository.removeUserIdFromVisit(userId);
    }

    public List<VisitDTO> getVisitBookedByIndicatedUser(String userId) {

        return visitRepository.findVisitBookedByIndicatedUser(userId).stream()
                .map(v->mapper.visitTOVisitDTO(v))
                .collect(Collectors.toList());
    }

    public VisitDTO addVisit(CreateVisitDTO createVisitDTO) {
        Visit visit = mapper.visitDTOtoVisit(createVisitDTO);
        Visit savedVisit = visitRepository.save(visit);
        return mapper.visitTOVisitDTO(savedVisit);
    }

    public List<VisitDTO>getNextDayBookedVisits() {
        return visitRepository.findNextDayVisits().stream()
                .map(v->mapper.visitTOVisitDTO(v))
                .collect(Collectors.toList());
    }
}
