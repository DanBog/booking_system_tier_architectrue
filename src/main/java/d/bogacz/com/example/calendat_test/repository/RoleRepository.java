package d.bogacz.com.example.calendat_test.repository;

import d.bogacz.com.example.calendat_test.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByRole(String role);
}
