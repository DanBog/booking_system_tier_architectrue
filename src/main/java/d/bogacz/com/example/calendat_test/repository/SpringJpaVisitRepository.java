package d.bogacz.com.example.calendat_test.repository;

import d.bogacz.com.example.calendat_test.model.Visit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface SpringJpaVisitRepository extends JpaRepository<Visit, String> {

    @Query(value = "SELECT * from visit v where v.day_id = :day_id", nativeQuery = true)
    List<Visit> findVisitForSelectedDay(@Param("day_id") String dayId);

    @Query(value = "SELECT * from visit v where v.user_id is null", nativeQuery = true)
    List<Visit> findAllUnreservedVisit();

    @Modifying
    @Query(value = "update visit v set v.user_id = :user_Id where v.visit_id = :visit_Id", nativeQuery = true)
    void bookVisit(@Param("user_Id") String userId, @Param("visit_Id") String visitId);

    @Modifying
    @Query(value = "update visit v set v.user_id = null where v.user_id = :user_Id", nativeQuery = true)
    void removeUserIdFromVisit(@Param("user_Id") String userId);

    @Query(value = "SELECT * from visit v where v.user_id = :user_id", nativeQuery = true)
    List<Visit>findVisitBookedByIndicatedUser(@Param("user_id") String userId);

    @Query(value = "SELECT * from visit v where v.start_time > :startTimeVisits and v.finish_time < :finishTimeVisits and user_id is not null", nativeQuery = true)
    List<Visit> findNextDayVisits(@Param("startTimeVisits") LocalDateTime startTimeVisits, @Param("finishTimeVisits") LocalDateTime finishTimeVisits);
}
