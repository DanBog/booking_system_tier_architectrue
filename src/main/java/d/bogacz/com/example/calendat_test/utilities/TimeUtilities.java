package d.bogacz.com.example.calendat_test.utilities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeUtilities {

    public static int getCutentDayOfMonth() {
        LocalDate localDate = LocalDate.now();
        return localDate.getDayOfMonth();
    }

    public static int getCutentYear() {
        LocalDate localDate = LocalDate.now();
        return localDate.getYear();
    }

    public static LocalDate getCurentDate() {
        return LocalDate.now();
    }

    public static String conwertFromZonedDataTineToString(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return localDateTime.format(formatter);
    }

    public static LocalDateTime convertFromStringToZonedDateTime(String time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(time, formatter);
    }




}
