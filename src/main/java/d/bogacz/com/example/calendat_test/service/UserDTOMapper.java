package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.Role;
import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
class UserDTOMapper {

    public UserDTO userToUserDTO(User user) {

        // TODO: 2019-09-18 to optional  
//        List<String> visitsId = null;
//        if (user.getVisits() != null) {
//            visitsId = user.getVisits().stream().map(h -> h.getVisitId()).collect(Collectors.toList());
//        }
        Set<Integer> roleId = user.getRoles().stream().map(r -> r.getRoleId()).collect(Collectors.toSet());
        return new UserDTO(
                user.getId(),
                user.getEmail(),
                user.getName(),
                user.getLastName(),
                roleId
//                visitsId
        );
    }


    public User userDTOtoUser(CreateUserDTO createUserDTO) {

//        List<Visit> visitList = new ArrayList<>();
//        for (int i = 0; i < userDTO.getVisitListID().size(); i++) {
//            Visit visit = visitService.findById(userDTO.getVisitListID().get(i));
//            visitList.add(visit);
//        }
        Role role = new Role();
        role.setRoleId(2);
        role.setRole("Role_User");

        return User.builder()
                .id(String.valueOf(UUID.randomUUID()))
                .email(createUserDTO.getEmail())
                .name(createUserDTO.getName())
                .lastName(createUserDTO.getLastName())
                .roles(new HashSet<>(Arrays.asList(role)))
                .build();
    }
}

