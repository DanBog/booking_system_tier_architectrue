package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.model.UserRepository;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import d.bogacz.com.example.calendat_test.service.exception.UserNotFoundException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDTOMapper mapper;



    public UserDTO addUser(CreateUserDTO createUserDTO) {
        User user = mapper.userDTOtoUser(createUserDTO);
        User savedUser = userRepository.save(user);
        return mapper.userToUserDTO(savedUser);
    }

    public List<UserDTO> getAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream().map(u -> mapper.userToUserDTO(u)).collect(Collectors.toList());
    }

    public UserDTO findaUserById(String id) throws UserNotFoundException {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException());
        return mapper.userToUserDTO(user);
    }

    public UserDTO deleteUserById(String id) throws UserNotFoundException {

        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException());
        userRepository.delete(user);
        return mapper.userToUserDTO(user);
    }

}