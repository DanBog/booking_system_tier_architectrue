package d.bogacz.com.example.calendat_test.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Day {

    @Id
    private String id = String.valueOf(UUID.randomUUID());
    private LocalDate date;
    private LocalDateTime startVisitsTime;
    private LocalDateTime finishVisitsTime;
    @OneToMany
    @JoinColumn(name = "day_id")
    private List<Visit> visitList;

}
