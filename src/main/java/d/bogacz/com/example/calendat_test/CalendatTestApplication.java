package d.bogacz.com.example.calendat_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@EnableScheduling
public class CalendatTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalendatTestApplication.class, args);
	}

}
