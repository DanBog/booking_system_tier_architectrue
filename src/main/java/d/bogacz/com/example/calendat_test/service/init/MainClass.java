package d.bogacz.com.example.calendat_test.service.init;

import d.bogacz.com.example.calendat_test.emailSender.EmailSender;
import d.bogacz.com.example.calendat_test.model.Day;
import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.repository.DayRepository;
import d.bogacz.com.example.calendat_test.repository.RoleRepository;
import d.bogacz.com.example.calendat_test.repository.SpringJpaUserRepository;
import d.bogacz.com.example.calendat_test.service.*;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class MainClass {

    @Autowired
    private VisitCalendarFactory visitCalendarFactory;
    @Autowired
    private DayVisitService dayVisitService;
    @Autowired
    private VisitService visitService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleRepository roleReposiotry;
    @Autowired
    private UserService userService;
    @Autowired
    private SpringJpaUserRepository userRepository;
    @Autowired
    private DayRepository dayRepository;

    @Autowired
    private EmailSender emailSender;


    @PostConstruct
    public void init()  {

        roleService.createRole("Role_Admin", 1);
        roleService.createRole("Role_User", 2);

        CreateUserDTO userDTO = new CreateUserDTO();
        userDTO.setName("Daniel");
        userDTO.setLastName("Bogacz");
        userDTO.setEmail("danielbogacz@onet.eu");
        UserDTO userDTO1 = userService.addUser(userDTO);
        String userId = userDTO1.getUserId();
        System.out.println(userId);
        System.out.println(userDTO1);
        List<Day> dayList = visitCalendarFactory.createRestOfCurrentMonthDays();
        dayVisitService.setVisitsToDays(dayList);
        List<Visit> unreservedVisits = visitService.getAllUnreservedVisits();
        List<Visit> forUser = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            String id = unreservedVisits.get(i).getVisitId();
            visitService.bookVisit(userId, id);
        }
        CreateUserDTO userDTO22 = new CreateUserDTO();
        userDTO22.setName("Andrzej");
        userDTO22.setLastName("Nowak");
        userDTO22.setEmail("andrzej.nowak@wp.pl");
        UserDTO user1 = userService.addUser(userDTO22);
        System.out.println(user1);
    }
}