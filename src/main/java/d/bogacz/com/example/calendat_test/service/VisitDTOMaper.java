package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.model.UserRepository;
import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.service.dto.CreateVisitDTO;
import d.bogacz.com.example.calendat_test.service.dto.VisitDTO;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor
public class VisitDTOMaper {

    @Autowired
    private UserRepository userRepository;

    public VisitDTO visitTOVisitDTO(Visit visit) {

        Optional<User> userOptional = Optional.ofNullable(visit.getUser());

        if (userOptional.isPresent()) {
            return VisitDTO.builder()
                    .visitId(visit.getVisitId())
                    .startTime(visit.getStartTime())
                    .finishTime(visit.getFinishTime())
                    .userId(userOptional.get().getId())
                    .build();

        } else {
            return new VisitDTO(
                    visit.getVisitId(),
                    visit.getStartTime(),
                    visit.getFinishTime()
            );
        }
    }

    public Visit visitDTOtoVisit(CreateVisitDTO createisitDTO)  {

        Optional<User> optionalUser = userRepository.findById(createisitDTO.getUserId());
        User user = null;
        if (optionalUser.isPresent()) {
           user = optionalUser.get();
        }

        return Visit.builder()
                .visitId(String.valueOf(UUID.randomUUID()))
                .startTime(createisitDTO.getStartTime())
                .finishTime(createisitDTO.getFinishTime())
                .user(user)
                .build();
    }
}

