package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.emailSender.EmailSender;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import d.bogacz.com.example.calendat_test.service.dto.VisitDTO;
import d.bogacz.com.example.calendat_test.service.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MailRemainderService {

    private static final Logger LOG = LoggerFactory.getLogger(MailRemainderService.class);

    @Autowired
    private VisitService visitService;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailSender emailSender;

    @Scheduled(cron = "0 0 12 * * ?")
    public void SendNexDayVisitEmailRemainder() throws UserNotFoundException {

        List<VisitDTO> nextDayBookedVisits = visitService.getNextDayBookedVisits();

        for (VisitDTO v : nextDayBookedVisits
        ) {
           UserDTO userToInform = userService.findaUserById(v.getUserId());
            String userEmail = userToInform.getEmail();
            String subject = "Przypomninie o wizycie";
            String content = "Przypominamy o wizycie w w dniu " + v.getStartTime().getDayOfMonth() +"."+v.getStartTime().getMonthValue()+"."+v.getStartTime().getYear()+" o godz. : "+v.getStartTime().getHour()+"."+v.getStartTime().getMinute();
            emailSender.sendEmail(userEmail, subject, content);
            LOG.info("**** WYWOłANO > SendNexDayVisitEmailRemainder() dla klienta - imię:" +  userToInform.getName() + ", nazwisko: " + userToInform.getLastName());
        }
    }
}


