package d.bogacz.com.example.calendat_test.model;

import java.util.List;
import java.util.Optional;

public interface VisitRepository {

    List<Visit> findVisitForSelectedDay(String dayId);

    List<Visit> findAllUnreservedVisit();

    void bookVisit(String userId, String visitId);

    void removeUserIdFromVisit(String userId);

    List<Visit> findVisitBookedByIndicatedUser(String userId);

    Optional<Visit> findById(String visitId);

    List<Visit> findAll();

    void delete(Visit visit);

    Visit save(Visit visit);

    List<Visit> findNextDayVisits();
}










