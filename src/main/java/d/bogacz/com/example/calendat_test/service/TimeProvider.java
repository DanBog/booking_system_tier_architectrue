package d.bogacz.com.example.calendat_test.service;

import java.time.LocalDate;

public interface TimeProvider {

    LocalDate now();
}
