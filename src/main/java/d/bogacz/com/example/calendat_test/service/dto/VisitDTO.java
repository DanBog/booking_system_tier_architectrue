package d.bogacz.com.example.calendat_test.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VisitDTO {

    private String visitId;
    private LocalDateTime startTime;
    private LocalDateTime finishTime;
    private String userId;

    public VisitDTO(String visitId, LocalDateTime startTime, LocalDateTime finishTime) {
        this.visitId = visitId;
        this.startTime = startTime;
        this.finishTime = finishTime;

    }
}
