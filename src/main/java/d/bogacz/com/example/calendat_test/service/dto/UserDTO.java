package d.bogacz.com.example.calendat_test.service.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO {

    private String userId;
    private String email;
    private String name;
    private String lastName;
    private Set<Integer> rolesSetId;


}
