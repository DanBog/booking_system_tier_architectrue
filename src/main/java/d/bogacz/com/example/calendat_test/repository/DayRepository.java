package d.bogacz.com.example.calendat_test.repository;

import d.bogacz.com.example.calendat_test.model.Day;
import d.bogacz.com.example.calendat_test.model.Visit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DayRepository extends JpaRepository<Day, String> {

}
