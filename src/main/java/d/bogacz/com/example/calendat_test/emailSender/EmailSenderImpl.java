package d.bogacz.com.example.calendat_test.emailSender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@Service
public class EmailSenderImpl implements EmailSender {


    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendEmail(String to, String subject, String content) {
        MimeMessage mail = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(to);
            helper.setFrom("booking-system_calendar_test");
            helper.setSubject(subject);
            helper.setText(content);
        }
        catch (MessagingException e) {
            e.printStackTrace();

        }
        javaMailSender.send(mail);
    }
}
