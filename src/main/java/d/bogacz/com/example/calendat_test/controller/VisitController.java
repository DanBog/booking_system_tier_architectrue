package d.bogacz.com.example.calendat_test.controller;


import d.bogacz.com.example.calendat_test.service.VisitService;
import d.bogacz.com.example.calendat_test.service.dto.CreateVisitDTO;
import d.bogacz.com.example.calendat_test.service.dto.VisitDTO;
import d.bogacz.com.example.calendat_test.service.exception.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/visits")
@Controller
@AllArgsConstructor
public class VisitController {


    @Autowired
    private VisitService visitService;

    @GetMapping
    public List<VisitDTO> getAllVisits() {
        return visitService.getAllVisits();
    }

    @GetMapping("/{visitId}")
    public VisitDTO getVisitByLogin(@PathVariable String visitId) {
        return visitService.getVisitByLogin(visitId);

    }

    @GetMapping("/visits/{userId}")
    public List<VisitDTO> getVisitBookedByUser(@PathVariable String userId) {
        return visitService.getVisitBookedByIndicatedUser(userId);

    }


    @DeleteMapping("/{visitId}")
    public VisitDTO deleteVisitByLogin(@PathVariable String visitId) {
        return visitService.deleteVisitByLogin(visitId);
    }


    @PutMapping("/bookVisit{visitId}/{userId}")
    public VisitDTO bookVisit(@PathVariable String visitId, String userId) {
        return visitService.bookVisit(userId, visitId);
    }

    @PostMapping
    public VisitDTO addVisit(@RequestBody CreateVisitDTO createVisitDTO) throws UserNotFoundException {
        return visitService.addVisit(createVisitDTO);
    }

    @GetMapping("/nextDay")
    public List<VisitDTO> visitsBookedNextDay() {
        System.out.println("invoked method visitsBookedNextDay() from VisitController");
        return visitService.getNextDayBookedVisits();
    }
}
