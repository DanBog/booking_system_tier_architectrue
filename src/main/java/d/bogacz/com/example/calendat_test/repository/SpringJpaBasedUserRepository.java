package d.bogacz.com.example.calendat_test.repository;

import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class SpringJpaBasedUserRepository implements UserRepository {

    @Autowired
    private SpringJpaUserRepository springJpaUserRepository;

    @Override
    public User save(User user) {
        return springJpaUserRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return springJpaUserRepository.findAll();
    }

    @Override
    public Optional<User> findById(String id) {
        return springJpaUserRepository.findById(id);
    }

    @Override
    public void delete(User user) {
        springJpaUserRepository.delete(user);
    }
}
