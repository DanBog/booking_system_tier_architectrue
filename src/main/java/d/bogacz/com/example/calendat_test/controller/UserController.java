package d.bogacz.com.example.calendat_test.controller;


import d.bogacz.com.example.calendat_test.service.UserService;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import d.bogacz.com.example.calendat_test.service.exception.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/users")
@Controller
@AllArgsConstructor
public class UserController {




    @Autowired
    private UserService userService;

    @GetMapping()
    public List<UserDTO> getAllUser() {

        return userService.getAllUsers();
    }

    @PostMapping
    public UserDTO addUser(@RequestBody CreateUserDTO createUserDTO) {
        return userService.addUser(createUserDTO);
    }

    @DeleteMapping("/{id}")
    public UserDTO deleteUserByLogin(@PathVariable String id) throws UserNotFoundException {
        return userService.deleteUserById(id);
    }
}


