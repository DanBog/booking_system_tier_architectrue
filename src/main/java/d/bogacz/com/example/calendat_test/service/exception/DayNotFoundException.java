package d.bogacz.com.example.calendat_test.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DayNotFoundException extends RuntimeException {
}
