package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.Day;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Stopwatch;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class DefaultVisitCalendarFactoryTest {

    @Rule
    public Stopwatch stopwatch;

    private DefaultVisitCalendarFactory visitCalendarFactory;

    @Test
    public void as() throws Exception {
        System.out.println(LocalDateTime.now());

        Thread.sleep(5000);

        System.out.println(LocalDateTime.now());
    }

    @Test
    public void new_created_day_has_expected_date() throws Exception {

        LocalDate expectedDate = LocalDate.now();

        Day day = whenCreateDay(expectedDate);

        assertEquals(expectedDate, day.getDate());
    }

    private Day whenCreateDay(LocalDate expectedDate) {
        visitCalendarFactory = new DefaultVisitCalendarFactory(new FixedTimeProvider(expectedDate));
        return visitCalendarFactory.createDay();
    }

    @Test
    public void new_created_day_has_expected_date_testWithMock() throws Exception {

        LocalDate expectedDate = LocalDate.now();
        TimeProvider mockTimeProvider = Mockito.mock(TimeProvider.class);
        Mockito.when(mockTimeProvider.now()).thenReturn(expectedDate);
        visitCalendarFactory = new DefaultVisitCalendarFactory(mockTimeProvider);

        Day day = visitCalendarFactory.createDay();

        assertEquals("Date in day:", expectedDate, day.getDate());
    }
}
