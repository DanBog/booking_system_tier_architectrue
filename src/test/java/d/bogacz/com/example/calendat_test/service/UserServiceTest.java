package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.repository.SpringJpaUserRepository;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    UserService userService;

    @Autowired
    SpringJpaUserRepository userRepository;
    @Test
    public void saveUser_shouldSaveUserToDBO() throws Exception {

        CreateUserDTO exampleUser = new CreateUserDTO();
        exampleUser.setName("Adam");
        exampleUser.setLastName("Nowak");
        exampleUser.setEmail("adam.nowak@onet.eu");
        UserDTO user = userService.addUser(exampleUser);
        String exampleUserId = user.getUserId();
        when(userService.findaUserById(exampleUserId)).thenReturn(user);
        User userFromRepository = userRepository.getOne(exampleUserId);
        assertEquals("adam.nowak@onet.eu", userFromRepository.getEmail());
        assertEquals("Adam", userFromRepository.getName());
        assertEquals("Nowak", userFromRepository.getLastName());
    }
}
