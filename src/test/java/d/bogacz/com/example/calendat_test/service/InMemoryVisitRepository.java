package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.model.VisitRepository;

import java.util.*;

public class InMemoryVisitRepository implements VisitRepository {

    private Set<Visit> visits = new HashSet<>();

    @Override
    public List<Visit> findVisitForSelectedDay(String dayId) {
        return null;
    }

    @Override
    public List<Visit> findAllUnreservedVisit() {
        List<Visit> unreservedVisit = new ArrayList<>();
        for (Visit v : visits
        ) {
            if (v.getUser() == null) {
                unreservedVisit.add(v);
            }
        }
        return unreservedVisit;
    }

    @Override
    public void bookVisit(String userId, String visitId) {

        User user = new User();
        user.setId(userId);

        for (Visit v : visits
        ) {
            if (v.getVisitId().equals(visitId)) {
                v.setUser(user);
                break;
            }
        }
    }

    @Override
    public void removeUserIdFromVisit(String userId) {
        for (Visit v : visits
        ) {
            if (v.getUser() != null) {
                if (v.getUser().getId().equals(userId)) {
                    v.setUser(null);
                }
            }
        }
    }

    @Override
    public List<Visit> findVisitBookedByIndicatedUser(String userId) {

        List<Visit> visitsBookedByindicatedUser = new ArrayList<>();
        for (Visit v : visits) {
            if (v.getUser() != null) {
                if (v.getUser().getId().equals(userId)) {
                    visitsBookedByindicatedUser.add(v);
                }
            }
        }
        return visitsBookedByindicatedUser;
    }

    @Override
    public Optional<Visit> findById(String visitId) {
        return visits.stream().filter(u -> u.getVisitId().equals(visitId)).findFirst();
    }


    @Override
    public List<Visit> findAll() {
        return new ArrayList<>(visits);
    }

    @Override
    public void delete(Visit visit) {
        visits.remove(visit);
    }

    @Override
    public Visit save(Visit visit) {
        visits.add(visit);
        return visit;
    }

    // TODO: 2019-11-07  
    @Override
    public List<Visit> findNextDayVisits() {
        return null;
    }
}
