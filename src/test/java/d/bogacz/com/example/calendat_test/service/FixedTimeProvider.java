package d.bogacz.com.example.calendat_test.service;

import lombok.AllArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
public class FixedTimeProvider implements TimeProvider {

    private LocalDate localDate;

    @Override
    public LocalDate now() {
        return localDate;
    }
}
