package d.bogacz.com.example.calendat_test.utils;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class TimeMeasureRule implements TestRule {

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() {
                long startTime = System.currentTimeMillis();
                long endTime = System.currentTimeMillis();
                System.out.println(String.format("Time of test: %s ms)", endTime - startTime));
            }
        };
    }
}
