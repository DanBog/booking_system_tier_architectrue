package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.controller.UserController;
import d.bogacz.com.example.calendat_test.controller.VisitController;
import d.bogacz.com.example.calendat_test.model.UserRepository;
import d.bogacz.com.example.calendat_test.model.VisitRepository;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.dto.CreateVisitDTO;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import d.bogacz.com.example.calendat_test.service.dto.VisitDTO;
import d.bogacz.com.example.calendat_test.service.exception.UserNotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class VisitServiceNoSpringTest {

    private VisitController visitController;

    private UserController userController;


    @Before
    public void init() {
        VisitRepository visitRepository = new InMemoryVisitRepository();
        UserRepository userRepository = new InMemoryUserRepository();
        VisitService visitService = new VisitService(visitRepository, new VisitDTOMaper(userRepository));
        visitController = new VisitController(visitService);

        UserService userService = new UserService(
                userRepository,
                new UserDTOMapper()
        );
        userController = new UserController(userService);


    }

    @Test
    public void added_visit_is_returned_same_with_id() throws UserNotFoundException {

        //given

        LocalDateTime startVisitTime = LocalDateTime.of(2019, 12, 12, 9, 00);
        LocalDateTime finishVisitTime = LocalDateTime.of(2019, 12, 12, 9, 30);

        CreateVisitDTO createVisitDTO = CreateVisitDTO.builder()
                .startTime(startVisitTime)
                .finishTime(finishVisitTime)
                .build();

        //when
        visitController.addVisit(createVisitDTO);

        //then

        List<VisitDTO> visitDTOList = visitController.getAllVisits();
        assertThat(visitDTOList).hasSize(1);
        VisitDTO visitDTO = visitDTOList.get(0);
        assertThat(visitDTO.getVisitId()).isNotNull();
        assertThat(visitDTO.getFinishTime().equals(finishVisitTime));
        assertThat(visitDTO.getStartTime().equals(startVisitTime));
        assertThat(visitDTO.getUserId() == null);
    }

    @Test
    public void returning_visit_booked_by_selected_user() throws UserNotFoundException {

        //given

        CreateUserDTO userDTO = CreateUserDTO.builder()
                .email("ASD@as.com")
                .name("Adam")
                .lastName("Nowak")
                .build();

        LocalDateTime startVisitTime = LocalDateTime.of(2019, 12, 12, 9, 0);
        LocalDateTime finishVisitTime = LocalDateTime.of(2019, 12, 12, 9, 30);


        CreateVisitDTO createVisitDT01 = CreateVisitDTO.builder()
                .startTime(startVisitTime)
                .finishTime(finishVisitTime)
                .build();

        CreateVisitDTO createVisitDT02 = CreateVisitDTO.builder()
                .startTime(startVisitTime.plusMinutes(30))
                .finishTime(finishVisitTime.plusMinutes(30))
                .build();

        CreateVisitDTO createVisitDT03 = CreateVisitDTO.builder()
                .startTime(startVisitTime.plusMinutes(60))
                .finishTime(finishVisitTime.plusMinutes(60))
                .build();

        //when

        userController.addUser(userDTO);
        List<UserDTO> users = userController.getAllUser();
        String  userId = users.get(0).getUserId();
        createVisitDT01.setUserId(userId);
        createVisitDT02.setUserId(userId);

        visitController.addVisit(createVisitDT01);
        visitController.addVisit(createVisitDT02);
        visitController.addVisit(createVisitDT03);

        //then


        List<VisitDTO> visits = visitController.getAllVisits();
        assertThat(visits).hasSize(3);

        List<VisitDTO> bookedVisits =  visitController.getVisitBookedByUser(userId);
        assertThat(bookedVisits).hasSize(2);

    }


    @Test
    public void visit_booked_by_selected_user() {





    }
}

