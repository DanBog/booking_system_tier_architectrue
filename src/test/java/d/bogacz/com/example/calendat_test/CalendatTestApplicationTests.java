package d.bogacz.com.example.calendat_test;

import d.bogacz.com.example.calendat_test.controller.UserController;
import d.bogacz.com.example.calendat_test.repository.SpringJpaVisitRepository;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import d.bogacz.com.example.calendat_test.utils.TimeMeasureRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CalendatTestApplicationTests {

    @Rule
    public TimeMeasureRule timeRule = new TimeMeasureRule();

    @Autowired
    private UserController userController;

    @Autowired
    private SpringJpaVisitRepository springJpaVisitRepository;

    @Test
    public void for_empty_db_no_user_exists() {
        assertThat(userController.getAllUser()).isEmpty();
    }

    @Test
    public void added_user_is_returned_same_with_id() {
        //given
        CreateUserDTO requestedAddUser = CreateUserDTO.builder()
                .email("ASD@as.com")
                .name("Adam")
                .lastName("Nowak")
                .build();



        //when
        userController.addUser(requestedAddUser);

        //then
        List<UserDTO> users = userController.getAllUser();
        assertThat(users).hasSize(1);
        UserDTO user = users.get(0);
        assertThat(user.getUserId()).isNotNull();
        assertThat(user.getEmail()).isEqualTo("ASD@as.com");
        assertThat(user.getName()).isEqualTo("Adam");
        assertThat(user.getLastName()).isEqualTo("Nowak");
    }
}


