package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.model.UserRepository;

import java.util.*;

public class InMemoryUserRepository implements UserRepository {

    private Set<User> users = new HashSet<>();

    @Override
    public User save(User user) {
        users.add(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(users);
    }


    @Override
    public Optional<User> findById(String id) {
        return users.stream().filter(u -> u.getId().equals(id)).findFirst();
    }


    @Override
    public void delete(User user) {
        users.remove(user);
    }
}
