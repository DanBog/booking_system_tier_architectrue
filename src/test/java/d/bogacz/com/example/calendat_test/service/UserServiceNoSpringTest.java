package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.controller.UserController;
import d.bogacz.com.example.calendat_test.model.UserRepository;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.dto.UserDTO;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UserServiceNoSpringTest {

    private UserController userController;

    @Before
    public void init() {

        UserRepository userRepository = new InMemoryUserRepository();
        UserService userService = new UserService(
                userRepository,
                new UserDTOMapper()
        );
        userController = new UserController(userService);
    }

    @Test
    public void added_user_is_returned_same_with_id() {
        //given
        CreateUserDTO requestedAddUser = CreateUserDTO.builder()
                .email("ASD@as.com")
                .name("Adam")
                .lastName("Nowak")
                .build();

        //when
        userController.addUser(requestedAddUser);


        //then
        List<UserDTO> users = userController.getAllUser();
        assertThat(users).hasSize(1);

        UserDTO user = users.get(0);
        String userId = user.getUserId();
        assertThat(user.getUserId()).isNotNull();
        assertThat(user.getEmail()).isEqualTo("ASD@as.com");
        assertThat(user.getName()).isEqualTo("Adam");
        assertThat(user.getLastName()).isEqualTo("Nowak");
    }
}
