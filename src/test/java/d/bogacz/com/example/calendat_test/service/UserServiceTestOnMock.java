package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.model.UserRepository;
import d.bogacz.com.example.calendat_test.service.dto.CreateUserDTO;
import d.bogacz.com.example.calendat_test.service.exception.UserAlreadyExistsException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTestOnMock {

    private UserService userService;

    @Mock
    UserRepository usersRepository;

    private UserDTOMapper mapper = new UserDTOMapper();

    @Mock
    VisitService visitService;

    @Autowired
    UserDTOMapper userDTOMapper;

    @Before
    public void init() {
        userService = new UserService(usersRepository, mapper);
        Mockito.when(usersRepository.save(any(User.class))).thenReturn(new User());
    }

    @Test
    public void saveUser_shouldSaveUserToDBO() {

        CreateUserDTO createUserDTO = CreateUserDTO.builder()
                .email("adam.nowak@onet.eu")
                .lastName("Nowak")
                .name("Jacek")
                .build();


        //NullPointer??????!!!!!!
        userService.addUser(createUserDTO);

        Mockito.verify(usersRepository).save(any(User.class));

//        List<UserDTO> userList = userService.getAllUsers();
//
//        UserDTO userDTO = userList.get(0);
//
//        // TODO: 2019-09-01 dlaczego userFromRepository nie jest null gdy w metodzie addUser klasy UserService zakometnuje fragment kodu odpowiedzialny za zapis Usera do bazy?
//        assertEquals("adam.nowak@onet.eu", userDTO.getEmail());
//        assertEquals("Jacek", userDTO.getName());
//        assertEquals("Nowak", userDTO.getLastName());
//        System.out.println("ID userDTO " + userDTO.getUserId());
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void createUser_shouldThrownExceptionForExistingUser() throws Exception {
//        User user = new User("12345","adam.nowak@onet.eu","Adam", "Nowak", null,null);
//        userService.addUser(user);
    }

}





