My first simple "CRUD" project to present some skill.
Used Spring, JPA, Lombock.
Project based on tier architecture.
Application is dedicated to companies whose activity is based on regular contact with clients (for example: barbers, dentists, beauty-studios, etc.). It's allow Them to book visits without phone or personal contact with providers of these services.  
